<?php

/**
 * @package ccUtils
 * @author  Chris Moutsos <chris@contextualcode.com>
 * @date    07 Feb 2018
 **/

require 'autoload.php';
$cli = eZCLI::instance();
$cli->setUseStyles(true);

$scriptSettings                   = array();
$scriptSettings['description']    = 'Creates folders with names from a text file';
$scriptSettings['use-session']    = true;
$scriptSettings['use-modules']    = true;
$scriptSettings['use-extensions'] = true;

$script  = eZScript::instance($scriptSettings);
$script->startup();
$script->initialize();
$options = $script->getOptions('[containernodeid:][filename:]', 
                               '', 
                               array(
                                   'containernodeid' => 'The parent node ID to put the folders in.',
                                   'filename'        => 'The file to read children folder names from.'
                              )
);
if (!isset($options['containernodeid'])) {
    $cli->error('Please supply a `--containernodeid` argument.');
    $script->shutdown(1);
}
if (!isset( $options['filename'])) {
    $cli->error('Please supply a `--filename` argument.');
    $script->shutdown(1);
}

$ini           = eZINI::instance();
$userCreatorID = $ini->variable('UserSettings', 'UserCreatorID');
$user          = eZUser::fetch($userCreatorID);
if (($user instanceof eZUser) === false) {
    $cli->error('Cannot get user object by userID = "' . $userCreatorID . '". ( See site.ini [UserSettings].UserCreatorID )');
    $script->shutdown(1);
}
eZUser::setCurrentlyLoggedInUser($user, $userCreatorID);

$classIdentifier  = 'folder';
$containerNodeID  = $options['containernodeid'];
$folderNamesFile  = $options['filename'];
$containerNode    = eZContentObjectTreeNode::fetch($containerNodeID);

if ($containerNode instanceof eZContentObjectTreeNode === false) {
    $cli->error('Container node is not available.');
    $script->shutdown(1);
}

$folderNames = array();
$handler     = fopen($folderNamesFile, 'r');
while (($buffer = fgets($handler)) !== false) {
    $folderNames[] = $buffer;
}
fclose($handler);

$counter   = array(
    'created' => 0,
);
foreach ($folderNames as $folderName) {
    $params = array(
        'creator_id'       => $userCreatorID,
        'class_identifier' => $classIdentifier,
        'parent_node_id'   => $containerNode->attribute('node_id'),
        'attributes'       => array(
            'name'      => $folderName
        )
    );

    $newObject = eZContentFunctions::createAndPublishObject($params);

    if ($newObject instanceof eZContentObject) {
        $counter['created']++;
    }
}

$cli->output('Created: ' . $counter['created']);
$script->shutdown(0);
