<?php

/**
 * @package ccUtils
 * @author  Chris Moutsos <chris@contextualcode.com>
 * @date    07 Feb 2018
 **/

require 'autoload.php';
$cli = eZCLI::instance();
$cli->setUseStyles(true);

$scriptSettings                   = array();
$scriptSettings['description']    = 'Changes section for an object';
$scriptSettings['use-session']    = true;
$scriptSettings['use-modules']    = true;
$scriptSettings['use-extensions'] = true;

$script  = eZScript::instance($scriptSettings);
$script->startup();
$script->initialize();
$options = $script->getOptions();

$ini           = eZINI::instance();
$userCreatorID = $ini->variable('UserSettings', 'UserCreatorID');
$user          = eZUser::fetch($userCreatorID);
if (($user instanceof eZUser) === false) {
    $cli->error('Cannot get user object by userID = "' . $userCreatorID . '". ( See site.ini [UserSettings].UserCreatorID )');
    $script->shutdown(1);
}
eZUser::setCurrentlyLoggedInUser($user, $userCreatorID);

$objectID   = $options['arguments'][0];
$sectionID  = $options['arguments'][1];

$object = eZContentObject::fetch($objectID);

if ($object instanceof eZContentObject === false) {
    $cli->error('Object is not available.');
    $script->shutdown( 1 );
}

$section = eZSection::fetch($sectionID);

if ($section instanceof eZSection === false) {
    $cli->error('Section is not available.');
    $script->shutdown(1);
}

$section->applyTo($object);

$cli->output('Done.');

$script->shutdown(0);
