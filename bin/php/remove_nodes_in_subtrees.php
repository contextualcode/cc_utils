#!/usr/bin/env php
<?php

require 'autoload.php';

/** Script startup and initialization **/

$cli = eZCLI::instance();
$script = eZScript::instance( array( 'description' => ( "Remove certain nodes in certain subtrees\n" .
    "\n" .
    "remove_nodes_in_subtrees.php --do_remove" ),
    'use-session' => false,
    'use-modules' => true,
    'use-extensions' => true,
    'user' => true ) );

$script->startup();

$options = $script->getOptions( "[do_remove:][filename_nodeids:][filename_subtrees:]",
    "",
    array(
        'do_remove' => 'Whether to do the actual removing',
        'filename_nodeids'  => 'The file to read the image node ids from.',
        'filename_subtrees'  => 'The file to read the subtree node ids from.'
    ),
    false,
    array( 'user' => true ) );
$script->initialize();

if (!isset( $options['filename_nodeids'])) {
    $cli->error('Please supply a `--filename_nodeids` argument.');
    $script->shutdown(1);
}
if (!isset( $options['filename_subtrees'])) {
    $cli->error('Please supply a `--filename_subtrees` argument.');
    $script->shutdown(1);
}

$doRemove = false;
if (isset( $options['do_remove']) && ($options['do_remove'] === '1' || $options['do_remove'] === 'true')) {
    $doRemove = true;
}

function addSysInfoToMessage( $message ) {
    $date        = date( 'c' );
    $memoryUsage = number_format( memory_get_usage( true ) / ( 1024 * 1024 ), 2 );
    return '[' . $date . ', Memory usage: ' . $memoryUsage . '] ' . $message;
}

$subtreesToCheckFile = $options['filename_subtrees'];
$subtreesToCheck = array();
$handler = fopen($subtreesToCheckFile, 'r');
while (($buffer = fgets($handler)) !== false) {
    $subtreesToCheck[] = trim($buffer);
}
fclose($handler);

$nodeIdsToCheckFile = $options['filename_nodeids'];
$nodeIdsToCheck = array();
$handler = fopen($nodeIdsToCheckFile, 'r');
while (($buffer = fgets($handler)) !== false) {
    $nodeIdsToCheck[] = trim($buffer);
}
fclose($handler);

$total = count($nodeIdsToCheck);

$db = eZDB::instance();

foreach ($nodeIdsToCheck as $i => $nodeId) {
    $node = eZContentObjectTreeNode::fetch($nodeId);
    if (!$node) {
        continue;
    }
    $pathArray = $node->pathArray();
    $inSubtree = count(array_intersect($pathArray, $subtreesToCheck));
    if (!$inSubtree) {
        print_r(addSysInfoToMessage("[ $i / $total ] $nodeId not in a target subtree, skipping...\r\n"));
        continue;
    }

    if (!$doRemove) {
        print_r(addSysInfoToMessage("[ $i / $total ] Would remove $nodeId but do_remove is not true\r\n"));
        continue;
    }

    print_r(addSysInfoToMessage("[ $i / $total ] Removing $nodeId ...\r\n"));

    $object = $node->object();

    if (!$object) {
        continue;
    }

    $db->begin();
    $object->removeThis();
    $db->commit();

}

/** Shutdown script **/

$script->shutdown();
