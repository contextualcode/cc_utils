<?php

/**
 * @package ccUtils
 * @author  Chris Moutsos <chris@contextualcode.com>
 * @date    03 Apr 2020
 **/

require 'autoload.php';
$cli = eZCLI::instance();
$cli->setUseStyles(true);

$scriptSettings                   = array();
$scriptSettings['description']    = 'Reports usages of all custom tags in content';
$scriptSettings['use-session']    = true;
$scriptSettings['use-modules']    = true;
$scriptSettings['use-extensions'] = true;

$script  = eZScript::instance($scriptSettings);
$script->startup();
$script->initialize();
$options = $script->getOptions('[show_nodes][show_number:][filepath:][admin_url:][command_prefix:][show_number_admin_urls:]',
    '',
    array(
        'show_nodes' => 'Output nodes.',
        'show_number' => 'If verbose, number of items to show',
        'filepath' => 'Custom filepath for the report. Defaults to `/tmp/full_custom_tag_report.csv`',
        'admin_url' => 'Admin URL',
        'command' => 'Command to run the custom tag report. Defaults to `php -dmemory_limit=-1 ../ezpublish/console ezpublish:legacy:script extension/cc_utils/bin/php/full_custom_tag_report.php`',
    )
);
$verbose = (isset($options['show_nodes']) && $options['show_nodes']);
$filename = (isset($options['filepath']) && $options['filepath']) ? $options['filepath'] : '/tmp/full_custom_tag_report.csv';
$adminUrl = (isset($options['admin_url']) && $options['admin_url']) ? rtrim($options['admin_url'], '/') : false;
$command = (isset($options['command']) && $options['command']) ? $options['command'] : 'php -dmemory_limit=-1 ../ezpublish/console ezpublish:legacy:script extension/cc_utils/bin/php/custom_tag_report.php';
$showNumAdminUrls = (isset($options['show_number_admin_urls']) && $options['show_number_admin_urls']) ? $options['show_number_admin_urls'] : false;

$ini           = eZINI::instance();
$userCreatorID = $ini->variable('UserSettings', 'UserCreatorID');
$user          = eZUser::fetch($userCreatorID);
if (($user instanceof eZUser) === false) {
    $cli->error('Cannot get user object by userID = "' . $userCreatorID . '". ( See site.ini [UserSettings].UserCreatorID )');
    $script->shutdown(1);
}
eZUser::setCurrentlyLoggedInUser($user, $userCreatorID);


$cli->output("Starting...");

$contentIni = eZINI::instance('content.ini');
$customTagNames = $contentIni->variable('CustomTagSettings', 'AvailableCustomTags');
$headers = [
    'Custom Tag Name',
    'Total Nodes',
    'Node Ids',
];
if ($adminUrl) {
    $headers[] = 'Admin URLs';
}
$fieldsArr = [$headers];
foreach ($customTagNames as $i => $customTag) {
    $cli->output("[" . ($i+1) . "/" . count($customTagNames) . "]: " . $customTag . " ...");
    $thisCommand = $command . ' ';
    $thisCommand .= '--customtagname=' . $customTag . ' ';
    if ($verbose) {
        $thisCommand .= '--show_nodes ';
        if (isset($options['show_number']) && $options['show_number']) {
            $thisCommand .= '--show_number=' . $options['show_number'] . ' ';
        }
    }

    $cli->output("Executing `" . $thisCommand . "` ...");
    $output = null;
    exec($thisCommand, $output);
    $thisData = [
        $customTag,
        $output[2],
        $output[3],
    ];
    if ($adminUrl) {
        $nodeIds = array_filter(explode(';', $output[3]));
        if ($showNumAdminUrls) {
            $nodeIds = array_slice($nodeIds, 0, $showNumAdminUrls);
        }
        $adminUrls = '';
        foreach ($nodeIds as $nodeId) {
            $adminUrls .= $adminUrl . '/content/view/full/' . $nodeId . ';';
        }
        $thisData[] = $adminUrls;
    }
    $fieldsArr[] = $thisData;
}

$fp = fopen($filename, 'w');
foreach ($fieldsArr as $fields) {
    fputcsv($fp, $fields);
}
fclose($fp);

$cli->output("Report saved to " . $filename);

$script->shutdown(0);
